DNS Delegation
==============

Tracing NS records trying to understand DNS delegation better.

Root Servers
------------

The root servers will return the TLD records for any domain

```bash
dig A @m.root-servers.net www.example.daplie.me
```

```
; <<>> DiG 9.8.3-P1 <<>> A @m.root-servers.net www.aj.daplie.me
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 34843
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 5, ADDITIONAL: 10
;; WARNING: recursion requested but not available

;; QUESTION SECTION:
;www.aj.daplie.me.		IN	A

;; AUTHORITY SECTION:
me.			172800	IN	NS	c0.nic.me.
me.			172800	IN	NS	b0.nic.me.
me.			172800	IN	NS	a0.nic.me.
me.			172800	IN	NS	b2.nic.me.
me.			172800	IN	NS	a2.nic.me.

;; ADDITIONAL SECTION:
a0.nic.me.		172800	IN	A	199.253.59.1
a2.nic.me.		172800	IN	A	199.249.119.1
b0.nic.me.		172800	IN	A	199.253.60.1
b2.nic.me.		172800	IN	A	199.249.127.1
c0.nic.me.		172800	IN	A	199.253.61.1
a0.nic.me.		172800	IN	AAAA	2001:500:53::1
a2.nic.me.		172800	IN	AAAA	2001:500:47::1
b0.nic.me.		172800	IN	AAAA	2001:500:54::1
b2.nic.me.		172800	IN	AAAA	2001:500:4f::1
c0.nic.me.		172800	IN	AAAA	2001:500:55::1

;; Query time: 141 msec
;; SERVER: 202.12.27.33#53(202.12.27.33)
;; WHEN: Tue Oct  3 15:47:25 2017
;; MSG SIZE  rcvd: 343
```

GTLD Servers
------------

The GTLD Servers will return the glue records for the nameserver which hosts the SLD in question

```bash
dig A @b0.nic.me www.aj.daplie.me
```

```
; <<>> DiG 9.8.3-P1 <<>> A @b0.nic.me www.aj.daplie.me
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 52062
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 2, ADDITIONAL: 0
;; WARNING: recursion requested but not available

;; QUESTION SECTION:
;www.aj.daplie.me.		IN	A

;; AUTHORITY SECTION:
daplie.me.		86400	IN	NS	ns2.redirect-www.org.
daplie.me.		86400	IN	NS	ns1.redirect-www.org.

;; Query time: 29 msec
;; SERVER: 199.253.60.1#53(199.253.60.1)
;; WHEN: Tue Oct  3 15:48:41 2017
;; MSG SIZE  rcvd: 86
```

If the nameserver were "in bailiwick" then it's A/AAAA records would be returned in an additional section.

SLD Nameservers
---------------

There are three main conditions:

* Exists
* Doesn't exist (but lives here)
* Delegated

There's also the condition of *"doesn't exist (and doesn't live here)"*, but we'll save that for another day
(or try `dig @ns1.google.com A yahoo.com`).

### Exists

```bash
dig A @ns2.redirect-www.org www.aj.daplie.me
```

```
; <<>> DiG 9.8.3-P1 <<>> A @ns2.redirect-www.org www.aj.daplie.me
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 52373
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0
;; WARNING: recursion requested but not available

;; QUESTION SECTION:
;www.aj.daplie.me.		IN	A

;; ANSWER SECTION:
www.aj.daplie.me.	300	IN	A	45.56.59.142

;; Query time: 83 msec
;; SERVER: 66.172.33.29#53(66.172.33.29)
;; WHEN: Tue Oct  3 15:57:14 2017
;; MSG SIZE  rcvd: 50
```

### Doesn't Exist (but would)

```bash
dig A @ns2.redirect-www.org doesntexist.aj.daplie.me
```

```
; <<>> DiG 9.8.3-P1 <<>> A @ns2.redirect-www.org doesntexist.aj.daplie.me
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 19993
;; flags: qr aa rd; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 0
;; WARNING: recursion requested but not available

;; QUESTION SECTION:
;doesntexist.aj.daplie.me.	IN	A

;; AUTHORITY SECTION:
daplie.me.		1	IN	SOA	ns1.redirect-www.org. hostmaster.daplie.me. 2017020100 10800 3600 1209600 1800

;; Query time: 68 msec
;; SERVER: 66.172.33.29#53(66.172.33.29)
;; WHEN: Tue Oct  3 15:59:25 2017
;; MSG SIZE  rcvd: 109
```

### Delegated Subdomain

Should look something like this, I'm pretty sure:

```
;; QUESTION SECTION:
;john.daplie.me.		IN	A

;; AUTHORITY SECTION:
john.daplie.me.		86400	IN	NS	ns2.dns-host.org.
john.daplie.me.		86400	IN	NS	ns1.dns-host.org.
```

I think that in practice anything matching `*.john.daplie.me` would be delegated,
but I but you could do something weird like host `whatever.john.daplie.me` on the original
nameserver by A) answering to it directly on the main nameserver and B) delegating
from `whatever.john.daplie.me` back to the original nameserver in case the resolving
client makes intelligent assumptions and caching.

When a domain doesn't exist
---------------------------

### NXDOMAIN

This nameserver can respond for that domain, but no record exists

```
dig @ns1.google.com doesntexist.google.com
```

```
; <<>> DiG 9.8.3-P1 <<>> @ns1.google.com doesntexist.google.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 45549
;; flags: qr aa rd; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 0
;; WARNING: recursion requested but not available

;; QUESTION SECTION:
;doesntexist.google.com.		IN	A

;; AUTHORITY SECTION:
google.com.		60	IN	SOA	ns2.google.com. dns-admin.google.com. 170961396 900 900 1800 60

;; Query time: 50 msec
;; SERVER: 216.239.32.10#53(216.239.32.10)
;; WHEN: Wed Oct  4 01:14:09 2017
;; MSG SIZE  rcvd: 90
```

### REFUSED

This nameserver does not store records for that domain
(and would appreciated it if you didn't ask)

```bash
dig @ns1.google.com daplie.com
```

```
; <<>> DiG 9.8.3-P1 <<>> @ns1.google.com daplie.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 47317
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 0
;; WARNING: recursion requested but not available

;; QUESTION SECTION:
;daplie.com.			IN	A

;; Query time: 52 msec
;; SERVER: 216.239.32.10#53(216.239.32.10)
;; WHEN: Wed Oct  4 01:14:20 2017
;; MSG SIZE  rcvd: 28
```
