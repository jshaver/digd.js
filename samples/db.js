'use strict';

module.exports =
{
  "primaryNameservers": [ "localhost" ] // 'ns1.vanity-dns.org'
, "domains": [
    { "id": "example.com", "revokedAt": 0 }
  , { "id": "smith.example.com", "revokedAt": 0 }
  , { "id": "in-delegated.example.com", "revokedAt": 0 }
  , { "id": "john.smith.example.com", "revokedAt": 0, "vanityNs": [ "ns1.dns-server.net", "ns2.dns-server.net" ] }
    // test and probably remove
  //, { "id": "out-delegated.example.com", "revokedAt": 0 }
  ]
, "records": [
    // zone example.com should be able to have some records on its own
    { "zone": "example.com", "name": "example.com", "tld": "com", "sld": "example", "sub": ""
    , "type": "A", "address": "1.2.3.4", "aname": "fido.devices.example.com" }
  , { "zone": "example.com", "name": "example.com", "tld": "com", "sld": "example", "sub": ""
    , "type": "MX", "priority": 10, "exchange": "mxa.example.org" }
  , { "zone": "example.com", "name": "example.com", "tld": "com", "sld": "example", "sub": ""
    , "type": "MX", "priority": 10, "exchange": "mxb.example.org" }
  , { "zone": "example.com", "name": "example.com", "tld": "com", "sld": "example", "sub": ""
    , "type": "SRV", "priority": 10, "weight": 20, "port": 65065, "target": "spot.devices.example.com" }
  , { "zone": "example.com", "name": "example.com", "tld": "com", "sld": "example", "sub": ""
    , "type": "TXT", "data": [ "foo bar baz" ] }
  , { "zone": "example.com", "name": "example.com", "tld": "com", "sld": "example", "sub": ""
    , "type": "TXT", "data": [ "foo", "bar", "baz" ] }

    // A, CNAME, ANAME, MX, SRV, TXT
  , { "zone": "example.com", "name": "a.example.com", "tld": "com", "sld": "example", "sub": "a"
    , "type": "A", "address": "4.3.2.1" }
  , { "zone": "example.com", "name": "aaaa.example.com", "tld": "com", "sld": "example", "sub": "aaaa"
    , "type": "AAAA", "address": "::1" }
  , { "zone": "example.com", "name": "aname.example.com", "tld": "com", "sld": "example", "sub": "aname"
    , "type": "A", "aname": "amazon.com" }
  , { "zone": "example.com", "name": "devname.example.com", "tld": "com", "sld": "example", "sub": "devname"
    , "type": "A", "address": "1.2.3.4", "aname": "fido.devices.example.com" }
  , { "zone": "example.com", "name": "cname.example.com", "tld": "com", "sld": "example", "sub": "cname"
    , "type": "CNAME", "data": "example.com" } // TODO should return additional
  , { "zone": "example.com", "name": "mx.example.com", "tld": "com", "sld": "example", "sub": "mx"
    , "type": "MX", "priority": 10, "exchange": "mxa.example.org" }
  , { "zone": "example.com", "name": "mx.example.com", "tld": "com", "sld": "example", "sub": "mx"
    , "type": "MX", "priority": 10, "exchange": "mxb.example.org" }
  , { "zone": "example.com", "name": "srv.example.com", "tld": "com", "sld": "example", "sub": "srv"
    , "type": "SRV", "priority": 10, "weight": 20, "port": 65065, "target": "spot.devices.example.com" }
  , { "zone": "example.com", "name": "txt.example.com", "tld": "com", "sld": "example", "sub": "txt"
    , "type": "TXT", "data": [ "foo bar baz" ] }
  , { "zone": "example.com", "name": "mtxt.example.com", "tld": "com", "sld": "example", "sub": "mtxt"
    , "type": "TXT", "data": [ "foo", "bar", "baz" ] }
  , { "zone": "example.com", "type": "NS", "name": "ns.example.com"
    , "tld": "com", "sld": "example", "sub": "ns", "data": "ns1.vanity-dns.org" }
  , { "zone": "example.com", "type": "NS", "name": "ns.example.com"
    , "tld": "com", "sld": "example", "sub": "ns", "data": "ns2.vanity-dns.org" }

    // www., email., etc just for fun
  , { "zone": "example.com", "name": "www.example.com", "tld": "com", "sld": "example", "sub": "www"
    , "type": "A", "address": "1.2.3.4", "aname": "fido.devices.example.com" }
  , { "zone": "example.com", "name": "email.example.com", "tld": "com", "sld": "example", "sub": "email"
    , "type": "CNAME", "data": "mailgun.org" }

    // a wildcard domain
    // wild.example.com does NOT match
    // exists.wild.example.com DOES match, statically
    // doesntexist.wild.example.com DOES match, wildly
  , { "zone": "example.com", "name": "*.wild.example.com", "tld": "com", "sld": "example", "sub": "*.wild"
    , "type": "A", "address": "12.34.56.78" }
  , { "zone": "example.com", "name": "exists.wild.example.com", "tld": "com", "sld": "example", "sub": "exists.wild"
    , "type": "A", "address": "123.0.0.45" }

    // Out-delegated Domains
  , { "zone": "example.com", "type": "NS", "name": "out-delegated.example.com"
    , "tld": "com", "sld": "example", "sub": "out-delegated", "data": "ns1.vanity-dns.org" }
  , { "zone": "example.com", "type": "NS", "name": "out-delegated.example.com"
    , "tld": "com", "sld": "example", "sub": "out-delegated", "data": "ns2.vanity-dns.org" }

    // In-delegated Domains
  , { "zone": "example.com", "type": "NS", "name": "in-delegated.example.com"
    , "tld": "com", "sld": "example", "sub": "in-delegated", "data": "localhost" }

  , { "zone": "example.com", "name": "fido.devices.example.com", "tld": "com", "sld": "example", "sub": "fido.devices"
    , "device": "abcdef123"
    , "type": "ANAME", "address": "1.2.3.4" }

    // zone example.com can delegate smith.example.com to the same nameserver
    // (it's probably programmatically and politically simplest to always delegate from a parent zone)
    // Thought Experiment: could we delegate the root to a child? i.e. example.com -> www.example.com
    // to let someone exclusively "own" the root domain, but none of the children?
  , { "zone": "example.com", "type": "NS", "name": "smith.example.com"
    , "tld": "com", "sld": "example", "sub": "smith", "data": "ns1.vanity-dns.org" }

  , { "zone": "example.com", "name": "smith.example.com", "tld": "com", "sld": "example", "sub": "smith"
    , "type": "NS", "data": "ns2.vanity-dns.org" }

    //
    // now the zone "smith.example.com" can be independently owned (and delegated)
    // ... but what about email for john@example.com with john@example.com?
  , { "zone": "smith.example.com", "name": "smith.example.com", "tld": "example.com", "sld": "smith", "sub": ""
    , "type": "A", "address": "45.56.59.142", "aname": "rex.devices.smith.example.com" }

  , { "zone": "smith.example.com", "name": "www.smith.example.com", "tld": "example.com", "sld": "smith", "sub": "www"
    , "type": "CNAME", "data": "smith.example.com" }

  , { "zone": "smith.example.com", "name": "john.smith.example.com", "tld": "example.com", "sld": "smith", "sub": "john"
    , "type": "NS", "data": "ns1.vanity-dns.org" }

  , { "zone": "smith.example.com", "name": "john.smith.example.com", "tld": "example.com", "sld": "smith", "sub": "john"
    , "type": "NS", "data": "ns2.vanity-dns.org" }

    // there can be a wildcard, to which a delegation is the exception
  , { "zone": "smith.example.com", "name": "*.smith.example.com", "tld": "example.com", "sld": "smith", "sub": "*"
    , "type": "A", "address": "45.56.59.142", "aname": "rex.devices.smith.example.com" }

    // there can be an exception to the delegation
  , { "zone": "smith.example.com", "name": "exception.john.smith.example.com", "tld": "example.com", "sld": "smith", "sub": "exception.john"
    , "type": "A", "address": "45.56.59.142", "aname": "rex.devices.smith.example.com" }


    //
    // john.smith.example.com
    //
  , { "zone": "john.smith.example.com", "name": "john.smith.example.com", "tld": "smith.example.com", "sld": "john", "sub": ""
   , "type": "A", "address": "45.56.59.142", "aname": "rex.devices.smith.example.com" }
  ]
}
;
