#!/bin/bash

sudo adduser --home /opt/digd.js --gecos '' --disabled-password digd
sudo mkdir -p /opt/digd.js/ /srv/digd.js /var/log/digd.js
sudo mkdir -p /opt/digd.js /srv/digd.js
#chown -R $(whoami):$(whoami) /opt/digd.js /srv/digd.js
chown -R digd:digd /opt/digd.js /srv/digd.js

echo "v8.9.0" > /tmp/NODEJS_VER
export NODE_PATH=/opt/digd.js/lib/node_modules
export NPM_CONFIG_PREFIX=/opt/digd.js
curl -fsSL https://bit.ly/install-min-node -o ./install-node.sh.tmp
bash ./install-node.sh.tmp
rm ./install-node.sh.tmp
/opt/digd.js/bin/node /opt/digd.js/bin/npm install -g npm@4

git clone https://git.coolaj86.com/coolaj86/digd.js /opt/digd.js/lib/node_modules/digd.js
pushd /opt/digd.js/lib/node_modules/digd.js
  git checkout v1.1
  /opt/digd.js/bin/node /opt/digd.js/bin/npm install
popd

sudo rsync -v /opt/digd.js/lib/node_modules/digd.js/dist/etc/systemd/system/digd.js.service /etc/systemd/system/
sudo rsync -v /opt/digd.js/lib/node_modules/digd.js/samples/db.json /srv/digd.js/db.json
sudo ln -s /opt/digd.js/lib/node_modules/digd.js/bin/digd.js /opt/digd.js/bin/

sudo chown -R digd:digd /opt/digd.js/ /srv/digd.js /var/log/digd.js

sudo systemctl daemon-reload
sudo systemctl restart digd.js

dig @localhost -p 53 example.com

#sudo journalctl -xefu digd.js
sudo journalctl -xeu digd.js
