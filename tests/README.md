Tests todo:

A - SERVFAIL - should return empty response, ra 0
A - NXDOMAIN - should return SOA
A - NOERROR - should return A record
            - or delegated NS record

Record delegated to self should return SOA record rather than NS delegation

Casing should match on question section, always.

Casing should match on other sections if a direct match? (not required, but efficient for compression pointers)
  - www.EXample.COm + example.com = EXample.COm
  - EXample.COm + www.example.com = www.EXample.COm
  - (this should be taken care of by compression pointer logic, once implemented)

Send malformed packets (both as queries and as answers):
  - bad question count, answer count, etc
  - bad rdata (too long, too short)
  - proper packets, truncated
  - randomly twiddled bits
  - forward compression pointers
  - compression pointers to wrong bits (throw error on non-ascii / unsafe chars)

Test that ANY queries return records of all types matching the domain

Test that A queries only return A records, not others matching the domain

Test that A queries for ANAME-enabled records (but no address) recurse (regardless of general recursion settings).
  * 0-anames.example.com
  * 1-aname.example.com
  * 2-anames.example.com

Generally speaking test the cases of 0, 1, and 2 records of any given type (null case, single case, multi case)

### Variables

```
port=65053
ns=localhost

# For the sake of accuracy, it's most important to test with the standard unix dig tool
digcmd="dig"

# For the sake of completeness, it's important to test with our very own dig tool
#digcmd="node bin/dig.js"
```

### Run the server

```
# Serve:
node bin/digd.js +norecurse -p $port --input sample/db.json
```

### Manual Tests

```
# Sample Data:
#   no A records for out-delegated.example.com
#   two external NS records for delegted.example.com
#   zone example.com exists

# Test:
#   should return NS records in AUTHORITY section, nothing else
$digcmd @$ns -p $port A out-delegated.example.com
$digcmd @$ns -p $port ANY out-delegated.example.com

#   should return SOA records in AUTHORITY section, nothing else
$digcmd @$ns -p $port A in-delegated.example.com
$digcmd @$ns -p $port ANY in-delegated.example.com

#   should return NS records in ANSWER section, nothing else
$digcmd @$ns -p $port NS out-delegated.example.com
$digcmd @$ns -p $port NS in-delegated.example.com


# Sample Data:
#   two A records for example.com
#   no NS records

# Test:
#   should return records in ANSWER section, nothing else
$digcmd @$ns -p $port A example.com
$digcmd @$ns -p $port AAAA example.com
$digcmd @$ns -p $port CNAME example.com
$digcmd @$ns -p $port MX example.com
$digcmd @$ns -p $port SRV example.com
$digcmd @$ns -p $port TXT example.com
$digcmd @$ns -p $port ANY example.com

#   should return SOA records in AUTHORITY section, nothing else
$digcmd @$ns -p $port A doesntexist.example.com
$digcmd @$ns -p $port NS doesntexist.example.com


# Sample Data:
#   two A records for a.example.com
#   has **internal** NS records

# Test:
#   should return record of correct type in ANSWER section, nothing else
$digcmd @$ns -p $port A a.example.com
$digcmd @$ns -p $port AAAA aaaa.example.com
$digcmd @$ns -p $port CNAME cname.example.com
$digcmd @$ns -p $port A cname.example.com       # Special Case, should return CNAME record
$digcmd @$ns -p $port MX mx.example.com
$digcmd @$ns -p $port SRV srv.example.com
$digcmd @$ns -p $port TXT txt.example.com
$digcmd @$ns -p $port TXT mtxt.example.com

#   should return record of correct type in ANSWER section, and SOA / NS
$digcmd @$ns -p $port ANY a.example.com
$digcmd @$ns -p $port ANY aaaa.example.com
$digcmd @$ns -p $port ANY mx.example.com
$digcmd @$ns -p $port ANY srv.example.com
$digcmd @$ns -p $port ANY txt.example.com
$digcmd @$ns -p $port ANY mtxt.example.com

# Test:
#   all subdomains of a delegated domain should return NS for that domain
$digcmd @$ns -p 65053 ANY ns.example.com
$digcmd @$ns -p 65053 ANY foo.ns.example.com
$digcmd @$ns -p 65053 ANY bar.foo.ns.example.com

#   should return SOA record in AUTHORITY section, nothing else
$digcmd @$ns -p $port A doesntexist.a.example.com

#   should return NS records in ANSWER section, nothing else
$digcmd @$ns -p $port NS a.example.com

# Test:
#   wildcard record should match after static records
$digcmd @$ns -p $port ANY wild.example.com                      # no record
$digcmd @$ns -p $port ANY exists.wild.example.com               # static record, before wildcard
$digcmd @$ns -p $port ANY foo.exists.wild.example.com           # no record
$digcmd @$ns -p $port ANY doesntexist.wild.example.com          # single-level wildcard
$digcmd @$ns -p $port ANY alsedoesntexist.wild.example.com      # single-level wildcard
$digcmd @$ns -p $port ANY foo.doesntexist.wild.example.com      # no second-level wildcard

```
